$( document ).ready( function () {

	$("a.nav-link").bind("click", function(e) {
		e.preventDefault();
		var page = $(this).attr("href");
		$('#content').fadeOut('fast', function() {
			$(this).load(page);
		}).fadeIn('normal');
	});

});